package az.digirella.service;

import az.digirella.domain.ValCurs;
import az.digirella.dto.ValCursDto;
import az.digirella.exception.ValCursNotFoundException;
import az.digirella.repository.ValCursRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ValCursServiceTest {
    private static final String dummy_date = "16.03.2023";

    @InjectMocks
    private ValCursService valCursService;

    @Mock
    private ValCursRepository valCursRepository;

    @Mock
    private ModelMapper modelMapper;

    @Test
    void getCurrencyWhenValCursFound() {
        // Arrange
        ValCurs valCurs = new ValCurs();
        valCurs.setDate(dummy_date);
        ValCursDto valCursDto = new ValCursDto();
        valCursDto.setDate(dummy_date);
        when(valCursRepository.findByDate(dummy_date)).thenReturn(valCurs);
        when(modelMapper.map(valCurs, ValCursDto.class)).thenReturn(valCursDto);
        // Act
        ValCursDto result = valCursService.getCurrency(dummy_date);
        // Assert
        assertNotNull(result);
        assertThat(dummy_date).isEqualTo(result.getDate());
        verify(valCursRepository, times(1)).findByDate(dummy_date);
        verify(modelMapper, times(1)).map(valCurs, ValCursDto.class);
    }

    @Test
    void getCurrencyWhenValCursNotFound() {
        // Arrange
        when(valCursRepository.findByDate(anyString())).thenReturn(null);
        // Assert&Act
        assertThatThrownBy(() -> valCursService.getCurrency(dummy_date))
                .isInstanceOf(ValCursNotFoundException.class)
                .hasMessage("DATE-NOT-FOUND");
    }

    @Test
    public void deleteCurrencyByDateTest() {
        // arrange
        String date="2021-02-02";
        ValCurs valCurs = new ValCurs();
        valCurs.setDate(date);
        when(valCursRepository.findByDate(date)).thenReturn(valCurs);
        // act
        valCursService.deleteCurrencyByDate(date);
        // assert
        verify(valCursRepository, times(1)).delete(valCurs);
    }

    @Test
    public void deleteCurrencyByDateNotFoundTest() {
        // arrange
        when(valCursRepository.findByDate(dummy_date)).thenReturn(null);
        //assert&act
        assertThatThrownBy(() -> valCursService.deleteCurrencyByDate(dummy_date))
                .isInstanceOf(ValCursNotFoundException.class)
                .hasMessage("DATE-NOT-FOUND");
    }



}