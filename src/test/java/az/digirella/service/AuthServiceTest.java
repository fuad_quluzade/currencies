package az.digirella.service;

import az.digirella.domain.Authority;
import az.digirella.domain.Role;
import az.digirella.domain.User;
import az.digirella.dto.LoginRequest;
import az.digirella.dto.response.RegisterResponse;
import az.digirella.exception.ExistsByUsernameException;
import az.digirella.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Set;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AuthServiceTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private AuthService authService;

    @Mock
    public PasswordEncoder passwordEncoder;

    private User user;

    private LoginRequest loginRequest;

    @BeforeEach
    void setUp() {
        this.passwordEncoder = new BCryptPasswordEncoder();
        authService = new AuthService(userRepository, passwordEncoder);

        Authority authority = Authority.builder()
                .role(Role.USER)
                .build();

        user = User.builder()
                .username("testuser")
                .password(passwordEncoder.encode("test"))
                .isEnabled(true)
                .isCredentialsNonExpired(true)
                .isAccountNonLocked(true)
                .isAccountNonExpired(true)
                .authorities(Set.of(authority))
                .build();

        loginRequest = LoginRequest.
                builder()
                .username("testuser")
                .password(passwordEncoder.encode("test"))
                .build();
    }

    @Test
    void testSignUpSuccessful() {
        //Arrange
        when(userRepository.save(any())).thenReturn(any());
        //Act
        RegisterResponse registerResponse = authService.signUp(loginRequest);
        //Assert
        assertThat(registerResponse).isEqualTo(new RegisterResponse("Successfully Register"));
    }

    @Test
    void testSignUpWhenUserNameExist() {
        //Arrange
        when(userRepository.existsByUsername(any())).thenReturn(true);
        //Act&&Assert
        assertThatThrownBy(() -> authService.signUp(loginRequest))
                .isInstanceOf(ExistsByUsernameException.class)
                .hasMessage("EXISTS-BY-USERNAME");
    }


}