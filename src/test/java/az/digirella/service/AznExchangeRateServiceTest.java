package az.digirella.service;

import az.digirella.dto.DateAndValueDto;
import az.digirella.dto.ValueAndCodeDto;
import az.digirella.exception.ValueNotFountException;
import az.digirella.projection.StoredDateAndValueProjection;
import az.digirella.projection.StoredValueAndCodeProjection;
import az.digirella.repository.ValCursRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AznExchangeRateServiceTest {

    @Mock
    private ValCursRepository valCursRepository;

    @InjectMocks
    private AznExchangeRateService aznExchangeRateService;

    private String date;
    private String code;
    private String expectedValue;

    @BeforeEach
    void setUp() {
        date = "2023-03-18";
        code = "USD";
        expectedValue = "1.00";
    }

    @Test
    void testGetValueByDateAndCode() {
        //Arrange
        when(valCursRepository.findValue(date, code)).thenReturn(expectedValue);
        //Act
        String actualValue = aznExchangeRateService.getValueByDateAndCode(date, code);
        //Assert
        assertEquals(expectedValue, actualValue);
        verify(valCursRepository, times(1)).findValue(date, code);
    }

    @Test
    void testGetValueByDateAndCodeWithNullValue() {
        //Arrange
        when(valCursRepository.findValue(date, code)).thenReturn(null);
        //Act&&Assert
        assertThatThrownBy(() -> aznExchangeRateService.getValueByDateAndCode(date, code))
                .isInstanceOf(ValueNotFountException.class)
                .hasMessage("DATE-NOT-FOUND");
    }

    @Test
    public void testGetValueByDate() {
        // Arrange
        List<StoredValueAndCodeProjection> value1 = valCursRepository.findAllByDate(date);
        // Act
        List<ValueAndCodeDto> result = new AznExchangeRateService(valCursRepository).getValueByDate("2023-03-19");
        // Assert
        assertEquals(value1.size(), result.size());
        assertThat(value1).isNotNull();
        verify(valCursRepository, times(1)).findAllByDate(date);
    }

    @Test
    public void testGetValueByCode() {
        // Arrange
        List<StoredDateAndValueProjection> value1 = valCursRepository.findAllByCode(code);
        // Act
        List<DateAndValueDto> result = new AznExchangeRateService(valCursRepository).getValueByCode("2023-03-19");
        // Assert
        assertEquals(value1.size(), result.size());
        assertThat(value1).isNotNull();
        verify(valCursRepository, times(1)).findAllByCode(code);
    }
}