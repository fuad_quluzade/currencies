package az.digirella.client;

import az.digirella.dto.ValCursDto;

import java.io.IOException;

public interface CbarClient {

    ValCursDto getValCursByDate(String date) throws IOException, InterruptedException;
}
