package az.digirella.client.impl;

import az.digirella.client.CbarClient;
import az.digirella.dto.ValCursDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@Slf4j
@Service
public class CbarClientImpl implements CbarClient {

    private static final String FILE_FORMAT = ".xml";
    @Value("${cbar.url}")
    String cbarUrl;

    @Override
    public ValCursDto getValCursByDate(String date) throws IOException, InterruptedException {
        HttpClient httpClient = HttpClient.newBuilder().build();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(cbarUrl + date + FILE_FORMAT))
                .build();
        log.info("request {}", request);
        HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        ObjectMapper xmlMapper = new XmlMapper();
        return xmlMapper.readValue(response.body(), ValCursDto.class);
    }
}
