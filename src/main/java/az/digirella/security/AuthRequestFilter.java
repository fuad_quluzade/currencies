package az.digirella.security;

import az.digirella.security.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.security.core.Authentication;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Configuration
@Component
public class AuthRequestFilter extends OncePerRequestFilter {

    private final List<AuthService> authServiceList;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

         Optional<Authentication> authOptional=Optional.empty();

         for(AuthService authService: authServiceList){
             authOptional=authOptional.or(()->authService.getAuthentication(request));
         }
         authOptional.ifPresent(auth-> SecurityContextHolder.getContext().setAuthentication(auth));
         filterChain.doFilter(request, response);

    }
}