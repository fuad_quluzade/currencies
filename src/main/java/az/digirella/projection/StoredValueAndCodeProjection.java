package az.digirella.projection;

public interface StoredValueAndCodeProjection {
    String getValue();
    String getCode();
}
