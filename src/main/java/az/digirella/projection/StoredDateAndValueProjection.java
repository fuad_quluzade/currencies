package az.digirella.projection;

public interface StoredDateAndValueProjection {
    String getCode();
    String getDate();
    String getValue();

}
