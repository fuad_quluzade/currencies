package az.digirella.controller;

import az.digirella.dto.LoginRequest;
import az.digirella.dto.response.RegisterResponse;
import az.digirella.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthController {

    private final AuthService authService;

    @PostMapping("/sign-up")
    public RegisterResponse singUp(@Valid @RequestBody LoginRequest loginRequest) {
        return authService.signUp(loginRequest);
    }
}
