package az.digirella.controller;

import az.digirella.dto.DateAndValueDto;
import az.digirella.dto.ValueAndCodeDto;
import az.digirella.service.AznExchangeRateService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/azn-exchange")
@RequiredArgsConstructor
public class AznExchangeRateController {

    private final AznExchangeRateService aznExchangeRateService;

    @GetMapping("/{date}/{code}")
    public ResponseEntity<String> getValueByDateAndCode(@PathVariable String date, @PathVariable String code) {
        return ResponseEntity.ok(aznExchangeRateService.getValueByDateAndCode(date, code));
    }

    @GetMapping("/{date}")
    public ResponseEntity<List<ValueAndCodeDto>> getAllValueByDate(@PathVariable String date) {
        return ResponseEntity.ok(aznExchangeRateService.getValueByDate(date));
    }

    @GetMapping("/get-value")
    public ResponseEntity<List<DateAndValueDto>> getAllValueByCode(@RequestParam String code) {
        return ResponseEntity.ok(aznExchangeRateService.getValueByCode(code));
    }
}
