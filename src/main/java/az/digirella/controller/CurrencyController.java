package az.digirella.controller;

import az.digirella.dto.ValCursDto;
import az.digirella.dto.response.ApiResponse;
import az.digirella.service.ValCursService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/v1")
@RequiredArgsConstructor
public class CurrencyController {

    private final ValCursService valcursService;

    @GetMapping("/get-currency")
    public ResponseEntity<ValCursDto> getCurrencyByDate(String date) {
        return ResponseEntity.ok(valcursService.getCurrency(date));
    }

    @PostMapping("/add")
    public ResponseEntity<ApiResponse> addCurrency(@RequestParam String date) throws IOException, InterruptedException {
        return new ResponseEntity<ApiResponse>(valcursService.addCurrency(date), HttpStatus.CREATED);
    }

    @DeleteMapping({"/delete/{date}"})
    public void deleteByDate(@PathVariable String date) {
        valcursService.deleteCurrencyByDate(date);
    }
}
