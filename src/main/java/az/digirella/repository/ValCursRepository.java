package az.digirella.repository;

import az.digirella.domain.ValCurs;
import az.digirella.domain.Valute;
import az.digirella.projection.StoredDateAndValueProjection;
import az.digirella.projection.StoredValueAndCodeProjection;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface ValCursRepository extends JpaRepository<ValCurs, Long> {
    @EntityGraph(attributePaths = {"valType", "valType.valute"})
    ValCurs findByDate(@Param("date") String date);

    @Query(value = "select value from val_curs v inner join val_type vt on v.id = vt.val_curs_id\n" +
            "             inner join valute v2 on vt.id = v2.val_type_id\n" +
            "where date=? and code=?", nativeQuery = true)
    String findValue(String date, String code);

    @Query(value = "select v2.value as value,v2.code as code from val_curs v inner join val_type vt on v.id = vt.val_curs_id\n" +
            "                         inner join valute v2 on vt.id = v2.val_type_id\n" +
            "            where date= :date", nativeQuery = true)
    List<StoredValueAndCodeProjection> findAllByDate(String date);

    @Query(value = "select v2.code as code ,v.date as date,value from val_curs v inner join val_type vt on v.id = vt.val_curs_id\n" +
            "                         inner join valute v2 on vt.id = v2.val_type_id\n" +
            "            where   code= :code", nativeQuery = true)
    List<StoredDateAndValueProjection> findAllByCode(String code);
}
