package az.digirella.exception;


import static az.digirella.exception.CurrencyErrorCode.DATE_NOT_FOUND;

public class ValCursNotFoundException extends CurrencyGenericException {
    public ValCursNotFoundException(Object... arguments) {
        super(DATE_NOT_FOUND.code, DATE_NOT_FOUND.code, 400, arguments);
    }
}
