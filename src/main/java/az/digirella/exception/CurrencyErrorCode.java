package az.digirella.exception;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum CurrencyErrorCode {

    DATE_NOT_FOUND("DATE-NOT-FOUND"),
    EXISTS_BY_USERNAME("EXISTS-BY-USERNAME");
    public final String code;
}
