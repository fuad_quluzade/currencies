package az.digirella.exception;

import static az.digirella.exception.CurrencyErrorCode.EXISTS_BY_USERNAME;

public class ExistsByUsernameException extends CurrencyGenericException {
    public ExistsByUsernameException(Object... arguments) {
        super(EXISTS_BY_USERNAME.code, EXISTS_BY_USERNAME.code, 404, arguments);
    }
}
