package az.digirella.exception;

import static az.digirella.exception.CurrencyErrorCode.DATE_NOT_FOUND;

public class ValueNotFountException extends CurrencyGenericException{

    public ValueNotFountException(Object... arguments) {
        super(DATE_NOT_FOUND.code, DATE_NOT_FOUND.code, 404, arguments);
    }
}
