package az.digirella.service;

import az.digirella.dto.DateAndValueDto;
import az.digirella.dto.ValueAndCodeDto;
import az.digirella.exception.ValueNotFountException;
import az.digirella.projection.StoredDateAndValueProjection;
import az.digirella.projection.StoredValueAndCodeProjection;
import az.digirella.repository.ValCursRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class AznExchangeRateService {

    private final ValCursRepository valCursRepository;

    public String getValueByDateAndCode(String date, String code)  {
        log.info("getting value by date {} and code {}",date,code);
        String value= valCursRepository.findValue(date, code);
        if (value == null || value.isEmpty()) {
            throw new ValueNotFountException();
        }
        log.info("Value {}",value);
        return value;
    }

    public List<ValueAndCodeDto> getValueByDate(String date) {
        log.info("getting value by date {}",date);
        List<StoredValueAndCodeProjection> list = valCursRepository.findAllByDate(date);
        return list.stream().map(AznExchangeRateService::toDto).collect(Collectors.toList());
    }

    public List<DateAndValueDto> getValueByCode(String code) {
        log.info("getting value by code {}",code);
        List<StoredDateAndValueProjection> list = valCursRepository.findAllByCode(code);
        return list.stream().map(AznExchangeRateService::projectionToDto).collect(Collectors.toList());
    }


    private static ValueAndCodeDto toDto(StoredValueAndCodeProjection projection) {
        return ValueAndCodeDto.builder()
                .value(projection.getValue())
                .code(projection.getCode())
                .build();
    }

    private static DateAndValueDto projectionToDto(StoredDateAndValueProjection projection) {
        return DateAndValueDto.builder()
                .code(projection.getCode())
                .value(projection.getValue())
                .date(projection.getDate())
                .build();
    }
}
