package az.digirella.service;

import az.digirella.domain.Authority;
import az.digirella.domain.Role;
import az.digirella.domain.User;
import az.digirella.dto.LoginRequest;
import az.digirella.dto.response.RegisterResponse;
import az.digirella.exception.ExistsByUsernameException;
import az.digirella.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
@RequiredArgsConstructor
@Slf4j
public class AuthService {

    private final UserRepository userRepository;
    private final PasswordEncoder encoder;

    public RegisterResponse signUp(LoginRequest loginRequest){
     log.info("login with username {}",loginRequest.getUsername());
      if (userRepository.existsByUsername(loginRequest.getUsername())) {
            throw  new ExistsByUsernameException();
        }
        User user = createUser(loginRequest);
        userRepository.save(user);
        log.info("user created with username:{}",loginRequest.getUsername());
        return new RegisterResponse("Successfully Register");
    }

    private User createUser(LoginRequest loginRequest){
        Authority authority = new Authority();
        authority.setRole(Role.USER);
       return User.builder()
                .username(loginRequest.getUsername())
                .password(encoder.encode(loginRequest.getPassword()))
                .isAccountNonExpired(true)
                .isCredentialsNonExpired(true)
                .isEnabled(true)
                .isAccountNonLocked(true)
                .authorities(Set.of(authority))
                .build();

    }
}
