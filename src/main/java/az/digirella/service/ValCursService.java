package az.digirella.service;

import az.digirella.client.CbarClient;
import az.digirella.domain.ValCurs;
import az.digirella.dto.ValCursDto;
import az.digirella.dto.response.ApiResponse;
import az.digirella.exception.ValCursNotFoundException;
import az.digirella.repository.ValCursRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Objects;

@Service
@Slf4j
@RequiredArgsConstructor
public class ValCursService {

    private final ValCursRepository valCursRepository;
    private final CbarClient cbarClient;
    private final ModelMapper modelMapper;

    public ApiResponse addCurrency(String date) throws IOException, InterruptedException {
        log.info("add currency by date {} from cbar",date);
        ValCurs valCurs = valCursRepository.findByDate(date);
        if (Objects.nonNull(valCurs)) {
            log.info("Information is available for this date {}",date);
            return ApiResponse.ALREADY_EXIST;
        }
        ValCursDto valCursDto = cbarClient.getValCursByDate(date);
        valCursRepository.save(new ModelMapper().map(valCursDto, ValCurs.class));
        log.info("Information is added in system for this date {}",date);
        return ApiResponse.ADDED;
    }

    public ValCursDto getCurrency(String date) {
        log.info("getting currency by date {}",date);
        ValCurs valCurs = valCursRepository.findByDate(date);
        if (Objects.isNull(valCurs)) {
            throw new ValCursNotFoundException();
        }
        return modelMapper.map(valCurs, ValCursDto.class);
    }

    public void deleteCurrencyByDate(String date) {
        log.info("deleting currency by date {}",date);
        ValCurs valCurs = valCursRepository.findByDate(date);
        if (Objects.isNull(valCurs)) {
            throw new ValCursNotFoundException();
        }
        valCursRepository.delete(valCurs);
    }
}
