package az.digirella.domain;

public enum Role {
    ADMIN, USER
}