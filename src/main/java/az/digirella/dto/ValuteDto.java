package az.digirella.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;


@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ValuteDto {
     @JacksonXmlProperty(isAttribute = true, localName = "Code")
     String code;
     @JacksonXmlProperty(localName = "Nominal")
     String nominal;
     @JacksonXmlProperty(localName = "Name")
     String name;
     @JacksonXmlProperty(localName = "Value")
     String value;
}
