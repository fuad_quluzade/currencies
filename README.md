##### Follow these steps to start using the currencies.application #####

1.run 'docker compose up' in command line
then application create db and insert some tranzactions in postgres db
if you don't have postgres db please check http://localhost:8580/?server=db&username=postgres (password : admin)
system is PostgreSql db name is currencies

2.use postman_collection in postman package
you can also use swagger-ui : http://localhost:8080/swagger-ui.html

3.Collection of information on official exchange rates
You can collect information about official exchange rates only through static token

static token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwicm9sZSI6WyJBRE1JTiJdLCJpYXQiOjE1MTYyMzkwMjJ9.kia8Nz_bJOuZCoVzamtYAtZbI7IeEwx6lZolyS2Yuz0

also marked in application.yaml and added to the postman collection

4.Obtaining information about AZN exchange rates

you can obtaining information about AZN exchange rates only basic auth

you must  sign up first
AuthController
http://localhost:8080/currency/auth/sign-up   (POST)
{
"username":"master",
"password":"master14"
}


