FROM alpine:3.11.2
RUN apk add --no-cache openjdk11
COPY *.jar /app/app.jar
WORKDIR /app/
ENTRYPOINT ["java"]
CMD ["-jar", "/app/app.jar"]
